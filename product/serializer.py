from rest_framework import serializers
from product.models import Product
from django.core.exceptions import ValidationError

class ProductSerializer(serializers.Serializer):
  name        = serializers.CharField()
  description = serializers.CharField()
  price       = serializers.FloatField(default=0)

  def create_new_product(self, data):
    """
      Create new product

      Parameters:
      -----------
        data : dict
          the serializer data validated
      
      Returns:
      --------
        product: SQLPosgres record
          new product inserted database
    """
    product = Product(name=data['name'],description=data['description'],price=data['price'])
    product.save()
    return product
  
  def update_product(self,data):

    try:
      product = Product.objects.get(name=data['name'])
    except:
      ValidationError("Product does not exist")
    product.update(description=data['description'],price=data['price'])
    return product
    
  def delete_product(self,data):
    try:
      product = Product.objects.get(name=data['name'])
    except:
      ValidationError("Product does not exist")

    product.delete()
    