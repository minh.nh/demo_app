from django.db import models
from django.utils import timezone

class Product(models.Model):
  name        = models.CharField(max_length=50,unique=True)
  description = models.CharField(max_length=255)
  price       = models.FloatField(default=0)
  created_at  = models.DateField(default=timezone.now)