from django.urls import path
from product.views import ProductView,ProductCreate,ProductUpdate,ProductDelete,ProductDetailView

urlpatterns = [
  path("api/get_all", ProductView.as_view(), name="get_all"),
  path("api/product_detail/<int:pk>", ProductDetailView.as_view(), name="product_detail"),
  path("api/create_new", ProductCreate.as_view(), name="create_new"),
  path("api/update", ProductUpdate.as_view(), name="create_new"),
  path("api/delete", ProductDelete.as_view(), name="create_new"),
]
