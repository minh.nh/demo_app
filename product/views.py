from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from product.serializer import ProductSerializer
from product.models import Product

from django.core.exceptions import ValidationError


class ProductView(APIView):

  permission_classes = [IsAuthenticated]

  def get(self, request):
    """
      Get all products
    """
    get_all_product = Product.objects.values("name", "description", "price").all()

    return Response(data=get_all_product, status=status.HTTP_200_OK)

class ProductDetailView(APIView):
  
  permission_classes = [IsAuthenticated]

  def get(self, request, pk):
    """
     The product detail
     
     Parameters:
     -----------
      pk: int
        the id of the Product

     Returns:
     --------
    """
    product_detail = Product.objects.values("name", "description", "price").filter(id=pk).first()

    return Response(data=product_detail,status=status.HTTP_200_OK)

class ProductCreate(APIView):

  # We just want admin create
  permission_classes = [IsAdminUser,IsAuthenticated] 
 
  def post(self, request):
    """
      Create a new product
      
      Parameters:
      -----------
        name: str
          name of the product
        description: str
          description of the product
        price: float
          price of the product
      
      Returns:
      --------
    """
    serializer = ProductSerializer(data=request.data)
    #Validate payload from client request
    if not serializer.is_valid():
      raise ValidationError(serializer.errors)
    serializer.create_new_product(request.data)
    return Response(data={"message": "Create success"}, status=status.HTTP_200_OK)
  
class ProductUpdate(APIView):
  
  # We just want admin update
  permission_classes = [IsAdminUser,IsAuthenticated]
  
  def put(self, request):
    """
      Update the product

      Parameters:
      -----------
        name: str
          name of the product
        description: str
          description of the product
        price: float
          price of the product
      
      Returns:
      --------
    """

    serializer = ProductSerializer(data=request.data)
    if not serializer.is_valid():
      raise ValidationError(serializer.errors)
    serializer.update_product(request.data)

    return Response(data={"message": "Update success"}, status=status.HTTP_200_OK)
  
class ProductDelete(APIView):

  # We just want admin delete
  permission_classes = [IsAdminUser,IsAuthenticated]
  
  def delete(self, request):
    """
      Delete the product

      Parameters:
      -----------
        name: str
          name of the product
        description: str
          description of the product
        price: float
          price of the product
      
      Returns:
      --------
    """

    serializer = ProductSerializer(data=request.data)
    if not serializer.is_valid():
      raise ValidationError(serializer.errors)
    serializer.delete_product(request.data)

    return Response(data={"message": "Delete success"}, status=status.HTTP_200_OK)
