from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated

from order.serializer import OrderSerializer

from django.core.exceptions import ValidationError

class SubmitOrderApi(APIView):

  permission_classes = [IsAuthenticated]  

  def post(self, request):
    """
      Save order into database
      
      Parameters:
      -----------
        user_id: int 
          id of the user 
        list_order: list(dict)
          list order detali
      
    """
    serializer = OrderSerializer(data=request.data)

    if not serializer.is_valid():
      raise ValidationError(serializer.errors)
    serializer.save_order(serializer.data)

    return Response(data={"message": "Submit order sucess"},status=status.HTTP_200_OK)