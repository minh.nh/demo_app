from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from product.models import Product

class Order(models.Model):
  user_id      = models.ForeignKey(User,on_delete=models.CASCADE)
  total_prices = models.FloatField(default=0)  # the total prices of the order
  created_at   = models.DateField(default=timezone.now)

class OrderDetail(models.Model):
  product_id  = models.ForeignKey(Product, on_delete=models.CASCADE)
  order_id    = models.ForeignKey(Order, on_delete=models.CASCADE)
  unit        = models.IntegerField(default=1) # the number of product
  prices      = models.FloatField(default=0)   # the total prices of the order detail
  created_at  = models.DateField(default=timezone.now)
