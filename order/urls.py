from django.urls import path
from order.views import SubmitOrderApi

urlpatterns = [
  path("api/submit_order", SubmitOrderApi.as_view(), name="submit_order")
]
