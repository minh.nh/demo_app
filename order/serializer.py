from rest_framework import serializers

from order.models import Order, OrderDetail

from product.models import Product

from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from django.db import transaction,IntegrityError

class OrderItemSerializer(serializers.Serializer):
  product_id = serializers.IntegerField()
  prices     = serializers.FloatField()
  unit       = serializers.IntegerField(default=1)

class OrderSerializer(serializers.Serializer):
  user_id    = serializers.IntegerField()
  list_order = OrderItemSerializer(many=True)

  def save_order(self,data):
    
    user = User.objects.get(id = data['user_id'])
    total_prices = sum(map(lambda x: x['prices'],data['list_order']))
    try:
      #TODO: If in process save Order we have a errors then rollback db.
      with transaction.atomic():
        order = Order(user_id=user, total_prices=total_prices)
        # In django we have to save first to get the id so this way not make sense
        order.save()
        # We want to call db once therefore can bulk_create to save a list OrderDetail.
        OrderDetail.objects.bulk_create([OrderDetail(order_id=order, **{
          "product_id": Product.objects.get(id=order_detail['product_id']),
          "prices": order_detail['prices'],
          "unit": order_detail['unit']
        }) for order_detail in data['list_order']])
    except IntegrityError:
      raise ValidationError("We have errors in process save Order")
    

    
  
  

