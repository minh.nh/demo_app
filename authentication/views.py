from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.views import TokenObtainPairView

from django.core.exceptions import ValidationError
from django.contrib.auth import logout

from authentication.serializer import (
	RegisterSerializer,
	ChangePasswordSerializer,
	CustomTokenPairObtainSerializer
)


class LoginView(TokenObtainPairView):
	"""
	Takes a set of user credentials and returns an access and refresh JSON web
	token pair to prove the authentication of those credentials.
	"""

	serializer_class = CustomTokenPairObtainSerializer
	
	def post(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		return Response(serializer.validated_data, status=status.HTTP_200_OK)

class LogoutView(APIView):

	authentication_classes = ()
	permission_classes = ()

	def post(self, request):
		logout(request)
		return Response(status=status.HTTP_200_OK)

class RegisterView(APIView):

	authentication_classes = ()
	permission_classes = ()
	
	def post(self, request):
		serializer = RegisterSerializer(data=request.data)
		if not serializer.is_valid():
			raise ValidationError(serializer.errors)
		serializer.create(request.data)
		
		return Response(status=status.HTTP_200_OK)

class ChangePasswordView(APIView):

	authentication_classes = ()
	permission_classes = ()

	def post(self, request):
		serializer = ChangePasswordSerializer(data=request.data)
		if not serializer.is_valid():
			raise ValidationError(serializer.errors)
		serializer.save()

		return Response(status=status.HTTP_200_OK)