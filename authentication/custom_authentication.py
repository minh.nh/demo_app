from django.contrib.auth.backends import BaseBackend
from django.contrib.auth.models import User

class Mybackend(BaseBackend):
	def authenticate(self, request, username=None, password=None):
		username = request.data['username']
		password = request.data['password']
		try:
				user = User.objects.get(username=username)
		except User.DoesNotExist:
				print("User does not exist")
		if user.check_password(password):
			return user
	
	def get_user(self, user_id):
		try:
			return User.objects.get(pk=user_id)
		except User.DoesNotExist:
			return None