from django.urls import path
from .views import (
  LoginView,
  ChangePasswordView,
  LogoutView,
  RegisterView
)

urlpatterns = [
  path('api/login', LoginView.as_view(), name='login'),
  path('api/logout', LogoutView.as_view(), name='logout'),
  path('api/change_password', ChangePasswordView.as_view(), name='change_password'),
  path('api/register', RegisterView.as_view(), name='register'),
]
