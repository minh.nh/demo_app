from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password

from rest_framework import serializers
from rest_framework import exceptions
from rest_framework_simplejwt.serializers import TokenObtainSerializer
from rest_framework_simplejwt.tokens import RefreshToken

from authentication.custom_authentication import Mybackend

class CustomTokenObtainSerializer(TokenObtainSerializer):
    
	def validate(self, attrs):
		"""
			Validate user

			Parameters:
			-----------
				username: str,
				password: str,
			
			Returns:
			--------
		"""
		authenticate_kwargs = {
			"username": attrs["username"],
			"password": attrs["password"],
		}
		try:
			authenticate_kwargs["request"] = self.context["request"]
		except KeyError:
			pass

		self.user = Mybackend.authenticate(attrs,**authenticate_kwargs)
		if self.user is None or not self.user.is_active:
			raise exceptions.AuthenticationFailed(
				self.error_messages['no_active_account'],
				'no_active_account',
			)
		return {}

class CustomTokenPairObtainSerializer(CustomTokenObtainSerializer):
	@classmethod
	def get_token(cls, user):
		"""
			Get access_token and refresh_token 
		"""
		return RefreshToken.for_user(user)

	def validate(self, attrs):

		data = super().validate(attrs)
		refresh = self.get_token(self.user)
		data['access'] = str(refresh.access_token)
		data['refresh'] = str(refresh)
		
		return data

class RegisterSerializer(serializers.Serializer):
	username   = serializers.CharField()
	email      = serializers.EmailField(required=False)
	first_name = serializers.CharField(required=False)
	last_name  = serializers.CharField(required=False)
	password   = serializers.CharField()

	def validate(self, data):
		"""
			Check if username exist serializer will raise a validation error
			Parameters:
			-----------
				data: dicts
					the serializer data

			Returns:
			--------
				data: dicts
					the serializer data validated
		"""
		if User.objects.filter(username=data["username"]).exists():
			raise serializers.ValidationError("User does exist")
		return data
	
	def create(self, data):
		"""
			Create new user
			
			Parameters:
			-----------
			 data: dicts
			 	the serializer data validated
			
			Returns:
			--------
				user: SQLPosgres record.
					User objects created.
		"""
		user = User(username=data['username'], password=make_password(data['password']))
		user.save()
		return user


class ChangePasswordSerializer(serializers.Serializer):
	username         = serializers.CharField()
	password         = serializers.CharField()
	comfirm_password = serializers.CharField()

	def validate_username(self, username):
		"""
			Check if username does not exist serializer will raise a validation error

			Parameters:
			----------
			 username: str
				name of the user
			
			Returns:
			-------
			 username: str
			 	name of the user
		"""
		if not User.objects.filter(username=username).exists():
			raise serializers.ValidationError("User does not exist")
		return username
	
	def validate_password(self, password):
		"""
			Check if password is not enough 8 characters serializer will raise a validation error

			Parameters:
			----------
			 password: str
		    password of the user
			
			Return:
			-------
			 password: str
			  password of the user
		"""
		if len(password) < 8:
			raise serializers.ValidationError("Your password need to be at least 8 characters")
		return password

	def validate(self, data):
		"""
			Check if password not match comfirm_password serializer will raise a validation error

			Parameter:
			----------
			 data: dict
			  the serializer data
			
			Returns:
			--------
			 data: dict
			 	the serializer data validated
		"""
		if data["password"] != data["comfirm_password"]:
			raise serializers.ValidationError("Password confirm did not match")
		return data
	
	def save(self):
		"""
			Change password of the user
		"""
		user = User.objects.get(username=self.validated_data["username"])
		user.set_password(self.validated_data['password'])
		user.save()
		return user


